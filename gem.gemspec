
Gem::Specification.new do |s|
  s.name        = 'story-gen'
  s.version     = '0.1.8'
  s.date        = '2017-03-16'
  s.summary     = "Story generator"
  s.description = "Generate stories from descriptions based on Facts!"
  s.authors     = ["Various Furriness"]
  s.email       = 'various.furriness@gmail.com'
  s.files       = Dir["lib/**/*.rb"] +
                  ["README.md", ".yardopts", "yardopts_extra.rb", "NEWS"] +
                  Dir["sample/*"]
  s.executables = Dir["bin/*"].map { |f| File.basename(f) }
  s.license     = 'MIT'
  s.add_runtime_dependency 'parse-framework'
  s.add_runtime_dependency 'ffaker'
end
