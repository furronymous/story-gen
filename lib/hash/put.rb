
class Hash
  
  # 
  # Alias for {Hash#[]=}.
  # 
  # @return [Hash] self.
  # 
  def put(key, value)
    self[key] = value
    self
  end
  
end