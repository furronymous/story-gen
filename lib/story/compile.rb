# encoding: UTF-8
require 'parse'
require 'code'
require 'string/ru_downcase'
require 'object/to_rb'
require 'array/case_equal_fixed'
require 'array/to_h'
require 'array/chomp'
require 'any'

# To remove YARD warnings:
# @!parse
#   class Class
#   end
#   module Kernel
#     def eval(str)
#     end
#   end

class Story
  
  # @param [String] text
  # @param [String] file a file the +text+ is taken from.
  # @return [String] a {String} which {Kernel#eval}s to a {Class} which
  #   inherits {Story}.
  # @raise [Parse::Error]
  def self.compile(text, file = "-")
    #
    c = Parse.new.(text, file).to_code
    # 
    relation_id_to_var_id = Hash.new { |h, relation_id| h[relation_id] = h.size }
    relation_ids = lambda { relation_id_to_var_id.keys }
    relation_id_to_var_arg_valuesss_var = lambda do |relation_id|
      "@#{INTERNAL_VAR_PREFIX}var_arg_valuesss#{relation_id_to_var_id[relation_id]}"
    end
    relation_id_to_trigger_var = lambda do |relation_id|
      "@#{INTERNAL_VAR_PREFIX}trigger#{relation_id_to_var_id[relation_id]}"
    end
    #
    c = c.map_non_code_parts do |part|
      case part
      when VarArgValuesssVar then relation_id_to_var_arg_valuesss_var[part.relation_id]
      when TriggerVar then relation_id_to_trigger_var[part.relation_id]
      else raise "unknown non-code part type: #{part.inspect}"
      end
    end
    #
    relation_ids_code = relation_ids.().
      map { |relation_id| relation_id.join(" ") }.
      sort.
      to_rb
    #
    init_var_arg_valuesss_vars = relation_ids.().
      map do |relation_id|
        "#{relation_id_to_var_arg_valuesss_var[relation_id]} = Set.new  # #{relation_id}\n"
      end.
      join_code
    #
    c = code <<
      "require 'enumerable/empty'\n"<<
      "require 'set'\n"<<
      "require 'story'\n"<<
      "require 'enumerable/new'\n"<<
      "\n"<<
      "Class.new(Story) do\n"<<
        "def relations\n"<<
          relation_ids_code << "\n"<<
        "end\n"<<
        "protected\n"<<
        "def write0()\n"<<
          init_var_arg_valuesss_vars << "\n"<<
          c << "\n"<<
        "end\n"<<
      "end\n"
    #
    c.to_s
  end
  
  private
  
  INTERNAL_VAR_PREFIX = "__internal__"
  
  # @!visibility private
  module ::ASTNode
    
    INTERNAL_VAR_PREFIX = Story::INTERNAL_VAR_PREFIX
    
    protected
    
    # macro
    def internal_var(base_name)
      "#{INTERNAL_VAR_PREFIX}#{base_name}"
    end
    
    # wraps result of +f+ to:
    #   
    #   (
    #     begin
    #       (f result)
    #     rescue Exception => e
    #       raise Story::Error.new(pos, e.message)
    #     end
    #   )
    # 
    # Here +e+ is some internal variable (see {INTERNAL_VAR_PREFIX}) and
    # +pos+ is converted to {Story::Position}.
    # 
    # @param [Parse::Position] pos
    # @yieldreturn [Code]
    # @return [Code]
    #   
    def at(pos, &f)
      e = internal_var(:e)
      code << "(begin\n" <<
        f.() << "\n" <<
      "rescue Exception => #{e}\n" <<
        "raise Story::Error.new(Story::Position.new(#{pos.file.to_rb}, #{pos.line.to_rb}, #{pos.column.to_rb}), #{e}.message)\n" <<
      "end)"
    end
    
  end
  
  # @!visibility private
  VarArgValuesssVar = Struct.new :relation_id, :pos
  
  #@!visibility private
  TriggerVar = Struct.new :relation_id, :pos
  
  Var = ASTNode.new :name, :original_name do
    
    alias name_super name
    
    def name
      raise Parse::Error.new(pos, "variables with names starting with `#{ASTNode::INTERNAL_VAR_PREFIX}' are reserved for internal use") if name_super.start_with? ASTNode::INTERNAL_VAR_PREFIX
      name_super
    end
    
    def == other
      self.class == other.class and
      self.name == other.name
    end
    
    def === other
      self.class == other.class and
      self.name === other.name
    end
    
    def hash
      name.hash
    end
    
  end
  
  Above = ASTNode.new :var
  
  Asterisk = ASTNode.new
  
  # @!visibility private
  module Statement
  end
  
  Statement::Tell = ASTNode.new :parts do
    
    def to_code
      code << "(begin\n" <<
        parts.map do |part|
          code << "tell(" <<
            case part
            when String then part.to_rb
            when Var then at(part.pos) { part.name }
            when Above[Var] then at(part.pos) { part.var.name }
            end <<
          ")"
        end.join_code("\n") << "\n" <<
      "end)"
    end
    
  end
  
  Statement::SetFact = ASTNode.new :subexpr do
    
    def to_code
      # 
      var_arg_valuess = internal_var(:var_arg_valuess)
      # 
      case subexpr
      when FactExpr::Not then
        args = subexpr.subexpr.args
        reject = args.zip(0...args.size).
          map do |arg, i|
            case arg
            when Asterisk then
              nil
            when Var then
              "#{at(arg.pos) { arg.name }} == #{var_arg_valuess}[#{i}]"
            when Above[Var] then
              "#{at(arg.pos) { arg.var.name }} == #{var_arg_valuess}[#{i}]"
            else
              "#{arg.to_rb} == #{var_arg_valuess}[#{i}]"
            end
          end.
          compact
        reject =
          if reject.empty?
          then ".clear()"
          else ".reject! { |#{var_arg_valuess}| #{reject.join(" and ")} }"
          end
        code <<
          non_code(VarArgValuesssVar[subexpr.subexpr.relation_id, pos]) <<
          reject
      else
        args = subexpr.args
        args_code = args.zip(0...args.size).
          map do |arg, i|
            case arg
            when Asterisk then raise Parse::Error.new(arg.pos, "not supported")
            when Var then at(arg.pos) { arg.name }
            when Above[Var] then at(arg.pos) { arg.var.name }
            else arg.to_rb
            end
          end.
          join_code(", ")
        trigger_var = non_code(TriggerVar[subexpr.relation_id, pos])
        code <<
          "(if " << trigger_var << " then " << "\n"<<
            trigger_var << ".call(" << args_code << ")" << "\n"<<
          "else" << "\n" <<
            non_code(VarArgValuesssVar[subexpr.relation_id, pos]) <<
            ".add([" << args_code << "])" << "\n"<<
          "end)"
      end
    end
    
  end
  
  Statement::If = ASTNode.new :condition_and_then_s, :else_ do
    
    def to_code
      code << "if_(\n"<<
        "[\n" <<
          condition_and_then_s.map do |condition, then_|
            code << "[\n" <<
              condition.to_code << ",\n" <<
              "lambda do |" << condition.var_args.map(&:name).join_code(", ") << "|\n" <<
                then_.to_code << "\n" <<
              "end\n" <<
            "]"
          end.join_code(",\n") << "\n" <<
        "],\n" <<
        if else_ then
          code << "lambda { " << else_.to_code << " }\n"
        else
          "nil\n"
        end <<
      ")"
    end
    
  end
  
  Statement::When = ASTNode.new :condition, :action do
    
    def to_code
      case condition
      when FactExpr::Primary
        args_code = condition.args.
          map do |arg|
            case arg
            when Asterisk then nil
            when Var then arg.name
            when Above[Var] then arg.var.name
            else raise Parse::Error.new(arg.pos, "not supported")
            end
          end.
          compact.
          join_code(", ")
        code << "(" << non_code(TriggerVar[condition.relation_id, pos]) << " = " <<
          "lambda do |" << args_code << "|\n" <<
            action.to_code << "\n" <<
          "end\n" <<
        ")"
      else
        raise Parse::Error.new(condition.pos, "not supported")
      end
    end
    
  end
  
  Statement::Compound = ASTNode.new :statements do
    
    def to_code
      code << "(begin\n" <<
        (statements.map { |statement| statement.to_code << "\n" }.reduce(:<<) || "") <<
      "end)"
    end
    
  end
  
  Statement::While = ASTNode.new :condition, :body do
    
    def to_code
      o =
        case condition
        when FactExpr::Not then code << "while " << condition.subexpr.to_code << ".empty?"
        else code << "until " << condition.to_code << ".empty?"
        end
      code << "(" << o << "\n" <<
        body.to_code << "\n" <<
      "end)"
    end
    
  end
  
  Statement::ForAll = ASTNode.new :fact_expr, :body do
    
    def to_code
      ignored = internal_var(:ignored)
      code << "(" << fact_expr.to_code << ".\n" <<
        "to_a.\n" <<
        "each do |" << fact_expr.var_args.map(&:name).join_code(", ") << ", " << ignored << " = nil|\n" <<
          body.to_code << "\n" <<
        "end)"
    end
    
  end
  
  Statement::NTimes = ASTNode.new :range_begin, :range_end, :body do
    
    def to_code
      n_code =
        if range_begin == range_end
        then to_code_(range_begin)
        else code << "rand(" << to_code_(range_begin) << ".." << to_code_(range_end) << ")"
        end
      code << "(" << n_code << ".times do\n" <<
        body.to_code << "\n" <<
      "end)"
    end
    
    private
    
    def to_code_(range_part)
      to_code = lambda do |var, pos|
        at(pos) { code << "(must_be(" << var.name << ", Integer); " << var.name << ")" }
      end
      #
      case range_part
      when Integer then range_part.to_rb
      when Var then to_code.(range_part, range_part.pos)
      when Above[Var] then to_code.(range_part.var, range_part.pos)
      else raise Parse::Error.new(pos, "what?")
      end
    end
    
  end
  
  Statement::Either = ASTNode.new :substatements do
    
    def to_code
      code << "[\n" <<
        substatements.
          map { |substatement| code << "lambda { " << substatement.to_code << " }" }.
          join_code(",\n") << "\n" <<
      "].sample.()"
    end
    
  end
  
  Statement::Code = ASTNode.new :body do
    
    def to_code
      at(pos) { body }
    end
    
  end
  
  Program = ASTNode.new :statements do
    
    def to_code
      statements.
        map { |statement| statement.to_code << "\n" }.
        reduce(:<<)
    end
    
  end
  
  # @!visibility private
  module FactExpr
    
    # @method to_code
    #   @abstract
    #   @return [Code] a {Code} which evaluates to "var arg valuess" -
    #     {Enumerable} of values of {Var} arguments ({Enumerable} of
    #     {Array}s of {Object}s).
    
    # @return [Array<Story::Var>] {Var} args attached to {#to_code} with
    #   {#with_var_args}.
    def var_args
      self.to_code.metadata or raise "no var args attached to #{self.to_code.inspect}"
    end
    
    # @return [Hash<Story::Var, Integer>] {#var_args} along with their indexes.
    def var_args_indexes
      (var_args).zip(0...var_args.size).to_h
    end
    
    protected
    
    # @param [Array<Story::Var>] var_args
    # @param [Code] code
    # @return [Code] +code+ with +var_args+ attached to it as {Code#metadata}.
    def with_var_args(var_args, code)
      code.metadata(var_args)
    end
        
  end
  
  FactExpr::Primary = ASTNode.new :relation_id, :args do
    
    include FactExpr
    
    def to_code
      var_arg_valuess = internal_var(:var_arg_valuess)
      select = []; map = []; begin
        known_var_index = {}
        args.zip(0...args.size).each do |arg, i|
          case arg
          when Asterisk then
            # do nothing
          when Var then
            var = arg
            if known_var_index.has_key? var then
              select << "#{var_arg_valuess}[#{known_var_index[var]}] == #{var_arg_valuess}[#{i}]"
            else
              map << "#{var_arg_valuess}[#{i}]"
              known_var_index[var] = i
            end
          when Above[Var] then
            select << "#{at(arg.pos) { arg.var.name }} == #{var_arg_valuess}[#{i}]"
          else
            select << "#{arg.to_rb} == #{var_arg_valuess}[#{i}]"
          end
        end
        select =
          if select.empty?
          then ""
          else ".select { |#{var_arg_valuess}| #{select.join(" and ")} }"
          end
        map =
          ".map { |#{var_arg_valuess}| [#{map.join(", ")}] }"
      end
      with_var_args(
        args.select { |arg| arg.is_a? Var },
        code << "(" <<
          non_code(VarArgValuesssVar[relation_id, pos]) <<
            select <<
            map <<
        ")"
      )
    end
    
  end
  
  FactExpr::Not = ASTNode.new :subexpr do
    
    include FactExpr
    
    def to_code
      raise Parse::Error.new(pos, "not supported")
    end
    
  end
  
  FactExpr::And = ASTNode.new :e1, :e2 do
    
    include FactExpr
    
    def to_code
      #
      var_arg_valuess = internal_var(:var_arg_valuess)
      var_arg_valuess1 = internal_var(:var_arg_valuess1)
      var_arg_valuess2 = internal_var(:var_arg_valuess2)
      block = internal_var(:block)
      #
      case [e1, e2]
      when [any, FactExpr::Not]
        common_var_args = e1.var_args & e2.subexpr.var_args
        if common_var_args.empty? then
          with_var_args(
            e1.var_args,
            code << "("<<
              "if " << e2.subexpr.to_code << ".empty? "<<
              "then " << e1.to_code << " "<<
              "else [] "<<
              "end"<<
            ")"
          )
        else
          condition = common_var_args.
            map do |var_arg|
              "#{var_arg_valuess1}[#{e1.var_args_indexes[var_arg]}] == #{var_arg_valuess2}[#{e2.subexpr.var_args_indexes[var_arg]}]"
            end.
            join(" and ")
          with_var_args(
            e1.var_args,
            code << "(" <<
              e1.to_code << ".select do |#{var_arg_valuess1}| "<<
                "not (" << e2.subexpr.to_code << ".any? { |#{var_arg_valuess2}| " << condition << " }) " <<
              "end" <<
            ")"
          )
        end
      when [any, FactExpr::Select]
        [e2.e1, e2.e2].
          select { |operand| operand.is_a? Var }.
          each do |e2_var_arg|
            if not e1.var_args.include? e2_var_arg then
              raise Parse::Error.new(e2_var_arg.pos, "#{e2_var_arg.original_name} is not known in the left part")
            end
          end
        to_code = lambda do |operand|
          case operand
          when Var then code << "#{var_arg_valuess}[#{e1.var_args_indexes[operand]}]"
          when Above[Var] then at(operand.pos) { operand.var.name }
          else code << operand.to_rb
          end
        end
        with_var_args(
          e1.var_args,
          code << "(" <<
            e1.to_code << ".select do |#{var_arg_valuess}| "<<
              at(e2.pos) { to_code.(e2.e1) << e2.rb_op << to_code.(e2.e2) } << " "<<
            "end " <<
          ")"
        )
      when [any, any]
        var_args = e1.var_args | e2.var_args
        select = []; map = []; begin
          var_args.each do |var_arg|
            if e1.var_args.include?(var_arg) and e2.var_args.include?(var_arg) then
              select << "#{var_arg_valuess1}[#{e1.var_args_indexes[var_arg]}] == #{var_arg_valuess2}[#{e2.var_args_indexes[var_arg]}]"
              map << "#{var_arg_valuess1}[#{e1.var_args_indexes[var_arg]}]"
            elsif e1.var_args.include?(var_arg) then
              map << "#{var_arg_valuess1}[#{e1.var_args_indexes[var_arg]}]"
            elsif e2.var_args.include?(var_arg) then
              map << "#{var_arg_valuess2}[#{e2.var_args_indexes[var_arg]}]"
            end
          end
          select =
            if select.empty?
            then "true"
            else "#{select.join(" and ")}"
            end
          map =
            "[#{map.join(", ")}]"
        end
        with_var_args(
          var_args,
          code << "(" <<
            "Enumerable.new do |#{block}| "<<
              e1.to_code << ".each do |#{var_arg_valuess1}| "<<
                e2.to_code << ".each do |#{var_arg_valuess2}| "<<
                  "if #{select} then "<<
                    "#{block}.(#{map}) "<<
                  "end "<<
                "end "<<
              "end "<<
            "end"<<
          ")"
        )
      end
    end
    
  end
  
  FactExpr::Select = ASTNode.new :e1, :op, :e2 do
    
    include FactExpr
    
    def to_code
      to_code = lambda do |operand|
        case operand
        when Var then raise Parse::Error.new(operand.pos, "not supported")
        when Above[Var] then at(operand.pos) { operand.var.name }
        else code << operand.to_rb
        end
      end
      # 
      with_var_args(
        [],
        code << "(if " << to_code.(e1) << rb_op << to_code.(e2) << " then [[]] else [] end)"
      )
    end
    
    # @return [String] Ruby analogue of {#op}.
    def rb_op
      case op
      when "==", "!=", "<=", ">=", "<", ">" then op
      when "<>", "=/=" then "!="
      when "=" then "=="
      end
    end
    
  end
  
  FactExpr::Or = ASTNode.new :e1, :e2 do
    
    include FactExpr
    
    def to_code
      # 
      var_arg_valuess = internal_var(:var_arg_valuess)
      block = internal_var(:block)
      # 
      var_args = e1.var_args | e2.var_args
      enum_var_arg_valuess = lambda do |e|
        new_var_arg_valuess = var_args.map do |var_arg|
          if (index = e.var_args_indexes[var_arg])
          then "#{var_arg_valuess}[#{index}]"
          else "nil"
          end
        end
        e.to_code << ".map { |#{var_arg_valuess}| [#{new_var_arg_valuess.join(", ")}] }.each(&#{block})\n"
      end
      with_var_args(
        var_args,
        code << "(" <<
          "Enumerable.new do |#{block}| "<<
            enum_var_arg_valuess.(e1) <<
            enum_var_arg_valuess.(e2) <<
          "end" <<
        ")"
      )
    end
    
  end
  
  # @!visibility private
  class Parse < ::Parse
    
    # --------------
    # @!group Syntax
    # --------------
    
    rule :start do
      no_errors { wsc } and
      ss = many {
        s = statement and opt{semicolon} and s
      } and
      opt{dot} and
      Program[ss]
    end
    
    rule :statement do
      _{ statement_if } or
      _{ statement_when } or
      _{ statement_while } or
      _{ statement_for_all } or
      _{ statement_n_times } or
      _{ statement_code } or
      _{ statement_set_fact } or
      _{ statement_tell } or
      _{ statement_compound } or
      _{ statement_either }
    end
    
    rule :statement_either do
      either and s1 = statement and opt_c_or_s and sn = many {
        _or_ and s = statement and opt_c_or_s and s
      } and
      if sn.empty?
      then s1
      else _(Statement::Either[[s1, *sn]])
      end
    end
    
    def opt_c_or_s
      opt{ _{semicolon} or _{comma} }
    end
    
    rule :statement_tell do
      expect("\"smth\"") { no_errors {
        e = fact_expr_primary_or_statement_tell and e.is_a?(Statement::Tell) and e
      } }
    end
    
    rule :statement_set_fact do
      f = fact_expr_primary and _(Statement::SetFact[f])
    end
    
    rule :statement_if do
      # single `then' form
      _{
        _if_ and c = fact_expr and opt{comma} and _then_ and s1 = statement and opt{semicolon} and
        s2 = opt { otherwise and s = statement and opt{semicolon} and s } and
        _(Statement::If[ [[c, s1]], s2.first ])
      } or
      # multiple `then' form
      _{
        _if_ and colon and c_and_sn = many {
          (_{dash} or _{ word and rparen }) and c = fact_expr and opt{comma} and _then_ and s = statement and opt{semicolon} and [c, s]
        } and
        otherwise_s = opt {
          (_{dash} or _{ word and rparen }) and otherwise and s = statement and opt{semicolon} and s
        } and
        _(Statement::If[c_and_sn, otherwise_s.first])
      }
    end
    
    rule :statement_when do
      _when_ and f = fact_expr_primary and s = statement and
      _(Statement::When[f, s])
    end
    
    rule :statement_while do
      _while_ and f = fact_expr and s = statement and
      _(Statement::While[f, s])
    end
    
    rule :statement_for_all do
      _for_ and all and f = fact_expr and s = statement and
      _(Statement::ForAll[f, s])
    end
    
    rule :statement_n_times do
      val = lambda { _{number} or _{var} or _{above_var} }
      #
      range_begin = range_end = val.() and opt { ellipsis and range_end = val.() } and times and s = statement and
      _(Statement::NTimes[range_begin, range_end, s])
    end
    
    rule :statement_code do
      c = _code_ and
      act {
        begin
          RubyVM::InstructionSequence.compile(c)
        rescue SyntaxError => e
          raise Parse::Error.new(rule_start_pos, e.message)
        end
      } and
      _(Statement::Code[c])
    end
    
    rule :statement_compound do
      body = lambda {
        ss = many {
          s = statement and opt{semicolon} and s
        } and
        _(Statement::Compound[ss])
      }
      #
      _{
        colon and b = body.() and dot and b
      } or
      _{
        lparen and b = body.() and rparen and b
      }
    end
    
    rule :fact_expr do
      fact_expr110
    end
    
    rule :fact_expr110 do
      f = fact_expr100 and
      many { _or_ and f2 = fact_expr100 and f = _(FactExpr::Or[f, f2]) } and
      f
    end
    
    rule :fact_expr100 do
      f = fact_expr90 and
      many { _and_ and f2 = fact_expr90 and f = _(FactExpr::And[f, f2]) } and
      f
    end
    
    rule :fact_expr90 do
      _{ fact_expr80 } or
      _{ _not_ and f = fact_expr90 and _(FactExpr::Not[f]) }
    end
    
    rule :fact_expr80 do
      _{ lparen and f = fact_expr110 and rparen and f } or
      _{ fact_expr_select } or
      _{ fact_expr_primary }
    end
    
    rule :fact_expr_select do
      #
      operand = lambda { _{var} or _{value} or _{above_var} }
      #
      v1 = operand.() and op = comparison_op and v2 = operand.() and
      _(FactExpr::Select[v1, op, v2])
    end
    
    rule :fact_expr_primary do
      expect("Fact expression") { no_errors {
        f = fact_expr_primary_or_statement_tell and f.is_a?(FactExpr) and f
      } }
    end
    
    rule :fact_expr_primary_or_statement_tell do
      relation_id = []
      args = []
      negated = false
      # 
      _{
        many {
          _{ s = (_{dash} or _{other_char} or _{comma}) and act { relation_id << s } } or
          _{ a = asterisk and act { args << a; relation_id << :* } } or
          _{ _not_ and act { negated = !negated } } or
          _{ v = (_{value} or _{var} or _{above_var}) and act { args << v; relation_id << :* } } or
          _{
            w = (
              _{ word } or
              _{ _for_ and not_follows {all} } or
              _{ all }
            ) and
            act { relation_id << w.ru_downcase }
          }
        }
      } and
      not relation_id.empty? and
      act { relation_id.chomp!(",") } and
      if not negated and relation_id.all? { |p| p == :* } then
        _(Statement::Tell[args])
      else
        e = _(FactExpr::Primary[relation_id, args])
        if negated then e = _(FactExpr::Not[e]); end
        e
      end
    end
    
    rule :value do
      _{ string } or
      _{ nl } or
      _{ number }
    end
    
    # ----------
    # @!endgroup
    # ----------
    
    # ------------------------
    # @!group Lexical Analysis
    # ------------------------
    
    begin
      @@keyword_method_ids = []
    end
    
    # macro  
    def self.token(method_id, human_readable_description, &body)
      rule method_id do
        expect(human_readable_description) {
          no_errors {
            r = instance_eval(&body) and wsc and r
          }
        }
      end
    end
    
    # macro
    def self.simple_token(method_id, str)
      token method_id, "`#{str}'" do
        scan(str)
      end
    end
    
    # macro
    def self.keyword(method_id, human_readable_description, check_regexp, &return_value_f)
      return_value_f ||= lambda { |captured_string| captured_string }
      @@keyword_method_ids.push(method_id)
      token method_id, human_readable_description do
        t = word_keyword_or_var and check_regexp === t and return_value_f.(t)
      end
    end
    
    simple_token :lparen, "("
    simple_token :rparen, ")"
    simple_token :comma, ","
    simple_token :semicolon, ";"
    simple_token :colon, ":"
    simple_token :dot, "."
    simple_token :dash, "-"
    simple_token :ellipsis, "..."
    
    keyword :_for_, "`for'", /^([Ff]or|[Дд]ля)$/
    keyword :all, "`all'", /^(all|всех)$/
    keyword :_while_, "`while'", /^([Ww]hile|[Пп]ока)$/
    keyword :_if_, "`if'", /^([Ii]f|[Ее]сли)$/
    keyword :_when_, "`when'", /^([Ww]hen|[Кк]огда)$/
    keyword :_then_, "`then'", /^(then|то)$/
    keyword :otherwise, "`otherwise'", /^([Oo]therwise|[Ии]наче)$/
    keyword :_not_, "`not'", /^(not|не)$/
    keyword :either, "`either'", /^([Ee]ither|[Ии]ли|[Лл]ибо)$/
    keyword :_or_, "`or'", /^([Oo]r|[Ии]ли|[Лл]ибо)$/
    keyword :_and_, "`and'", /^(and|и)$/
    keyword :times, "`times'", /^(times|раза?)$/
    keyword :newline, "`newline'", /^(newline|nl)$/, &proc { "\n" }
    alias nl newline
    
    token :comparison_op, "comparison operator" do
      scan(/=\/=|=|!=|<>|<=|>=|<|>/)
    end
    
    token :asterisk, "`*'" do
      scan("*") and _(Asterisk.new)
    end
    
    token :other_char, "any of `#№@$%/[]{}'" do
      scan(/[\#\№\@\$\%\/\[\]\{\}]/)
    end
    
    token :number, "number" do
      n = scan(/-?\d+/) and Integer(n)
    end
    
    token :string, "string" do
      _{
        scan('"') and
        s = scan(/[^"]*/) and
        (scan('"') or raise Error.new(rule_start_pos, "`\"' missing at the end")) and
        s
      } or
      _{
        scan("'") and
        s = scan(/[^']*/) and
        (scan("'") or raise Error.new(rule_start_pos, %(<<'>> missing at the end))) and
        s
      }
    end
    
    token :_code_, "code" do
      p = pos and
      scan("```") and c = scan(/((?!```).)*/m) and (scan("```") or raise Parse::Error.new(p, "\"```\" missing at the end")) and
      c
    end
    
    token :var, "variable" do
      n = word_keyword_or_var and
      /^[_A-ZА-ЯЁ][_A-ZА-ЯЁ0-9]*$/ === n and
      _(Var[n.ru_downcase, n])
    end
    
    token :above_var, "^variable" do
      scan("^") and v = var and _(Above[v])
    end
    
    token :word, "word" do
      not_follows(:var, *@@keyword_method_ids) and word_keyword_or_var
    end
    
    rule :word_keyword_or_var do
      scan(/[a-zA-Zа-яёА-ЯЁ_](['\-]?[a-zA-Zа-яёА-ЯЁ0-9_]+)*/)
    end
    
    rule :whitespace_and_comments do
      many {
        _{ ws } or
        _{
          p = pos and
          scan("/*") and scan(/((?!\*\/).)*/m) and (scan("*/") or raise Error.new(p, "`*/' missing at the end"))
        } or
        _{
          p = pos and
          scan("(") and ws_opt and scan(/note|прим.|примечание/) and ws_opt and scan(":") and
          note_comment_body and
          (scan(")") or raise Error.new(p, "`)' missing at the end"))
        }
      }
    end
    
    alias wsc whitespace_and_comments
    
    rule :whitespace do
      scan(/\s+/)
    end
    
    alias ws whitespace
    
    rule :whitespace_opt do
      opt { ws }
    end
    
    alias ws_opt whitespace_opt
    
    rule :note_comment_body do
      many {
        _{ scan(/[^\(\)]+/m) } or
        _{ p = pos and scan("(") and note_comment_body and (scan(")") or raise Error.new(p, "`)' missing at the end")) }
      }
    end
    
    # ----------
    # @!endgroup
    # ----------
    
  end
  
end
