# encoding: UTF-8
require 'ffaker'

class Names
  
  # @return [Array<String>]
  def self.russian_male
    FFaker::NameRU::FIRST_NAMES[:male]
  end
  
  # @return [Array<String>]
  def self.russian_female
    FFaker::NameRU::FIRST_NAMES[:female]
  end
  
  # @return [Array<String>]
  def self.english_male
    FFaker::Name::FIRST_NAMES_MALE
  end
  
  # @return [Array<String>]
  def self.english_female
    FFaker::Name::FIRST_NAMES_FEMALE
  end
  
end
