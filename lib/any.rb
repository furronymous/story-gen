
# To disable YARD warnings:
# @!parse
#   class Object
#     def === other; end
#   end

# @return [Object] an {Object} which is {Object#===} to anything.
def any
  Object
end
