# encoding: UTF-8
require 'enumerable/empty'
require 'enumerable/sample'

# To remove YARD warnings:
# @!parse
#   class Proc
#   end

class Story
  
  # @param [IO] out
  # @return [void]
  # @raise [Story::Error]
  def write(out = STDOUT)
    @out = out
    write0()
  end
  
  # @!method relations
  #   @abstract
  #   @return [Array<String>] relations mentioned in this {Story}.
  
  Position = Struct.new :file, :line, :column
  
  class Error < Exception
    
    # @param [Story::Position] pos
    # @param [String] message
    def initialize(pos, message)
      super(message)
      @pos = pos
    end
    
    # @return [Story::Position]
    attr_reader :pos
    
  end
  
  protected
  
  # @!method write0
  #   @abstract
  #   
  #   Implementation of {#write}.
  #   
  #   @return [void]
  #   @raise [Story::Error]
  
  # chooses a random non-empty {Enumerable} from +enumerable_and_action_s+,
  # then chooses a random {Array} from that {Enumerable} and calls
  # the appropriate {Proc}, passing the chosen {Array} as its *parameters.
  # Or calls +else_action+ if all of the {Enumerable}s are empty.
  # 
  # @param [Array<Enumerable<Array<Object>>, Proc>] enumerable_and_action_s
  # @param [Proc<*Object>] else_action
  # @return [void]
  # 
  def if_(enumerable_and_action_s, else_action)
    x = enumerable_and_action_s.
      map do |enumerable_and_action|
        enumerable, action = *enumerable_and_action
        [enumerable.sample, action]
      end.
      reject do |sample, action|
        sample.nil?
      end.
      sample
    if x.nil? then
      else_action.() if else_action
    else
      data, action = *x
      action.(*data)
    end
  end
  
  # writes +msg+ to +out+ passed to {#write}.
  # 
  # @param [String] msg
  # @return [void]
  def tell(msg)
    @out.write(msg)
  end
  
  def must_be(x, clazz)
    raise "#{x.inspect} is not #{if /^[aeiou]/ === clazz.to_s.downcase then "an" else "a" end} #{clazz}" unless x.is_a? clazz
  end
  
end
