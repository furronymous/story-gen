
# You can use +redo+ inside the passed block.
def once
  break yield while true
end
