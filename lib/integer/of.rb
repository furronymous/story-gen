
class Integer
  
  # @return [Array] an {Array} of n results of call to +block+, where n is self.
  def of(&block)
    r = []
    self.times { r.push(block.()) }
    return r;
  end
  
end
