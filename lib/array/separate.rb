
class Array
  
  # @param [Object, nil] separator
  # @return [Array] [self_1, separator, self_2, separator, ..., self_n].
  def separate(separator)
    (self.map { |item| [item, separator] }.reduce(:concat) or [1])[0...-1]
  end
  
end
