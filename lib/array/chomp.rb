
class Array
  
  # if +item+ == {Array#last} then {Array#pop}s it; otherwise do nothing
  # 
  # @return [self]
  # 
  # @example
  # 
  #   [1, 2, 3].chomp!(3)  #=> [1, 2]
  #   [1, 2, 3].chomp!(2)  #=> [1, 2, 3]
  # 
  def chomp!(item)
    self.pop if item == self.last
    return self
  end
  
  # @return [Array] copy of this {Array} processed as per {#chomp!}.
  def chomp(item)
    self.dup.chomp!(item)
  end
  
end
