
unless Array.method_defined? :to_h
  
  class Array
    
    # @return [Hash]
    def to_h
      self.reduce({}) { |h, e| h[e.first] = e.last; h }
    end
    
  end
  
end
