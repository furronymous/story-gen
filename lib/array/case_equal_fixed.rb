
class Array
  
  def === other
    self.size == other.size and
      self.zip(other).all? { |e1, e2| e1 === e2 }
  end
  
end
