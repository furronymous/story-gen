# encoding: UTF-8
require 'set'
require 'once'
require 'names'

# Infinite set of unique names.
class UniqueNames
  
  # @param [Array<String>] base_names base names which will be used to
  #   generate the {UniqueNames}.
  def initialize(base_names)
    @base_names = base_names
    @postfix_id = 0
    @deleted_names = Set.new
    reinit_current_names
  end
  
  # @return [UniqueNames]
  def self.russian_male
    UniqueNames.new(Names.russian_male)
  end
  
  # @return [UniqueNames]
  def self.russian_female
    UniqueNames.new(Names.russian_female)
  end
  
  # @return [UniqueNames]
  def self.english_male
    UniqueNames.new(Names.english_male)
  end
  
  # @return [UniqueNames]
  def self.english_female
    UniqueNames.new(Names.english_female)
  end
  
  # @param [String] name
  # @return [self]
  def delete(name)
    @deleted_names.add name
    return self
  end
  
  # @return [String]
  def pop
    once do
      n = @current_names.pop + postfix
      if @current_names.empty?
        @postfix_id += 1
        reinit_current_names
      end
      redo if @deleted_names.include? n
      return n
    end
  end
  
  private
  
  def reinit_current_names
    @current_names = @base_names.shuffle
  end
  
  def postfix
    if @postfix_id == 0
    then ""
    else "-#{@postfix_id}"
    end
  end
  
end
