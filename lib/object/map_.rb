
class Object
  
  # @return [Object] result of +f+ applied to self.
  def map_(&f)
    f.(self)
  end
  
end
