require 'set'
require 'enumerable/lazy'
require 'enumerable/new'
require 'hash/put'

class Fact
  
  # @param [Object] relation_id
  # @param [*(:'*' | Symbol | Object)] args
  # @return [Fact]
  def self.[](relation_id, *args)
    select = []; remove = []; map = []; arg_names = []; can_add = true
    begin
      known_var_index = {}
      args.zip(0...args.size).
        each do |arg, i|
          case arg
          when :'*' then
            can_add = false
          when Symbol then
            var = arg
            if known_var_index.has_key? var then
              select << (x = "arg_values[#{known_var_index[var]}] == arg_values[#{i}]")
              remove << x
            else
              map << "arg_values[#{i}]"
              arg_names << var
              known_var_index[var] = i
            end
            can_add = false
          when Object then
            select << (x = "args[#{i}] == arg_values[#{i}]")
            remove << x
          end
        end
      select =
        if select.empty?
        then ""
        else "select { |arg_values| #{select.join(" and ")} }."
        end
      remove =
        if remove.empty? then ".clear"
        else ".reject! { |arg_values| #{reject.join(" and ")} }"
        end
      map =
        "map { |arg_values| [#{map.join(", ")}] }"
    end
    Fact.new(
      arg_names,
      eval(
        "lambda do |context| "+
          "context[relation_id]."+
            "lazy."+
            select +
            map +
        " end "
      ),
      if can_add then
        lambda { |context| context[relation_id].add(args) }
      else
        nil
      end,
      eval("lambda { |context| context[relation_id]#{remove} }")
    )
  end
  
  # @!visibility private
  def initialize(arg_names, arg_valuess_f, true_f = nil, false_f = nil)
    @arg_names = arg_names
    @arg_valuess_f = arg_valuess_f
    @true_f = true_f
    @false_f = false_f
  end
  
  # @return [Array<Symbol>]
  attr_reader :arg_names
  
  # @param [Context] context
  # @return [Enumerable<Array<Object>>] values of named arguments passed to
  #   {Fact::[]} which this {Fact} is {#true?} for (in the +context+).
  def arg_valuess(context)
    @arg_valuess_f.(context)
  end
  
  # makes this {Fact} to be {#true?} in +context+.
  # 
  # It modifies +context+. It is not applicable to some {Fact}s.
  # 
  # @param [Context] context
  # @return [void]
  # 
  def true!(context)
    raise "not applicable" unless @true_f
    @true_f.(context)
  end
  
  # @param [Context] context
  # @return [Boolean]
  def true?(context)
    not false?(context)
  end
  
  # makes this {Fact} to be {#false?} in +context+.
  # 
  # It modifies +context+. It is not applicable to some {Fact}s.
  # 
  # @param [Context] context
  # @return [void]
  # 
  def false!(context)
    raise "not applicable" unless @false_f
    @false_f.(context)
  end
  
  # @param [Context] context
  # @return [Boolean]
  def false?(context)
    arg_valuess.(context).empty?
  end
  
  # -------------------
  # @!group Combinators
  # -------------------
  
  # @param [Fact, Fact::Not, Fact::Select] f2
  # @return [Fact]
  def & f2
    case f2
    when Fact::Select
      if not (s = f2.arg_names - f1.arg_names).empty? then
        raise "#{s.map(&:to_s).join(", ")} in the right part is/are not known in the left part"
      end
      Fact.new(
        f1.arg_names,
        lambda { |context| f1.arg_valuess(context).select(&f2.criteria) }
      )
    when Fact::Not
      common_arg_names = f1.arg_names & f2.subfact.arg_names
      if common_arg_names.empty? then
        Fact.new(
          f1.arg_names,
          lambda do |context|
            if f2.subfact.true?(context) then []
            else f1.arg_valuess(context)
            end
          end
        )
      else
        Fact.new(
          f1.arg_names,
          eval(
            "lambda do |context| "+
              "f1.arg_valuess(context).select do |f1_arg_values| "+
                "not (" +
                  "f2.subfact.arg_valuess(context).any? do |f2_subfact_arg_values| "+
                    common_arg_names.
                      map do |arg_name|
                        "f1_arg_values[#{f1.arg_indexes[arg_name]}] == f2_subfact_arg_values[#{f2.subfact.arg_indexes[arg_name]}]"
                      end.
                      join(" and ") + " " +
                  "end "+
                ") "+
              "end " +
            "end "
          )
        )
      end
    when Fact
      arg_names = f1.arg_names | f2.arg_names
      select = []; map = []; begin
        arg_names.each do |arg_name|
          if f1.arg_indexes.key?(arg_name) and f2.arg_indexes.key?(arg_name) then
            select << "f1_arg_values[#{f1.arg_indexes[arg_name]}] == f2_arg_values[#{f2.arg_indexes[arg_name]}]"
            map << "f1_arg_values[#{f1.arg_indexes[arg_name]}]"
          elsif f1.arg_indexes.key?(arg_name) then
            map << "f1_arg_values[#{f1.arg_indexes[arg_name]}]"
          elsif f2.arg_indexes.key?(arg_name) then
            map << "f2_arg_values[#{f2.arg_indexes[arg_name]}]"
          end
        end
        select =
          if select.empty? then "true"
          else "#{select.join(" and ")}"
          end
        map =
          "[#{map.join(", ")}]"
      end
      Fact.new(
        arg_names,
        eval(
          "lambda do |context| "+
            "Enumerable.new do |block| "+
              "f1.arg_valuess(context).each do |f1_arg_values| "+
                "f2.arg_valuess(context).each do |f2_arg_values| "+
                  "if #{select} then "+
                    "block.(#{map}) "+
                  "end "+
                "end "+
              "end "+
            "end "+
          "end "
        )
      )
    end
  end
  
  # @param [Fact] f2
  # @return [Fact]
  def | f2
    arg_names = f1.arg_names | f2.arg_names
    map1 = nil; map2 = nil; begin
      map = lambda do |arg_indexes|
        eval "lambda do |arg_values|
          [#{
            arg_names.map do |arg_name|
              if arg_indexes.key?(arg_name) then
                "arg_values[#{arg_indexes[arg_name]}]"
              else
                "nil"
              end
            end.
            join(", ")
          }]
        end"
      end
      map1 = map.(f1.arg_indexes)
      map2 = map.(f2.arg_indexes)
    end
    Fact.new(
      arg_names,
      lambda do |context|
        Enumerable.new do |block|
          f1.arg_valuess(context).map(&map1).each(&block)
          f2.arg_valuess(context).map(&map2).each(&block)
        end
      end
    )
  end
  
  # @return [Fact::Not]
  def !@
    Fact::Not.new(self)
  end
  
  class Not
    
    # @param [Fact] subfact
    def initialize(subfact)
      raise "#{Fact} is expected" unless subfact.is_a? Fact
      @subfact = subfact
    end
    
    # @return [Fact]
    attr_reader :subfact
    
  end
  
  class Select
    
    # @param [*Symbol] arg_names
    # @param [Proc<*Object, Boolean>] criteria
    def initialize(*arg_names, &criteria)
      @arg_names = arg_names
      @criteria = criteria
    end
    
    # @return [Array<Symbol>]
    attr_reader :arg_names
    
    # @return [Proc<*Object, Boolean>]
    attr_reader :criteria
    
  end
  
  # ----------
  # @!endgroup
  # ----------
  
  # 
  class Context
    
    def initialize()
      @arg_valuesss = Hash.new { |arg_valuesss, name| arg_valuesss[name] = Set.new }
    end
    
    # @api private
    # @note used by {Fact} only
    # 
    # @param [Object] relation_id
    # @return [Set<Array<Object>>]
    def [](relation_id)
      @arg_valuesss[relation_id]
    end
    
  end
  
  protected
  
  # @!visibility private
  # 
  # @return [Hash<Symbol, Integer>]
  # 
  def arg_indexes
    arg_names.zip(0...arg_names.size).reduce({}) do |h, arg_name_and_arg_index|
      arg_name, arg_index = *arg_name_and_arg_index
      h.put(arg_name, arg_index)
    end
  end
  
  private
  
  def f1
    self
  end
  
end

# ctx = Fact::Context.new
# Fact["loves", "John", "Liza"].true!(ctx)
# Fact["yiffs", "Sam", "Liza"].true!(ctx)
# Fact["yiffs", "John", "Liza"].true!(ctx)
# Fact["loves", "John", "Johanna"].true!(ctx)
# p((Fact["loves", :x, :y] & Fact["yiffs", :x, :'*']).arg_valuess(ctx).to_a)
# p((Fact["loves", :x, :y] | Fact["yiffs", :x, :y]).arg_valuess(ctx).to_a)
# p((Fact["loves", :x, :y] & !Fact["yiffs", :'*', :y]).arg_valuess(ctx).to_a)
# x = Fact["loves", :x, :y] & Fact::Select.new(:x, :y) { |x, y| x != "John" }
# p x.arg_names
# p x.arg_valuess(ctx).to_a
# Fact["loves", "Kris", "Diana"].true!(ctx)
# Fact["loves", "John", :'*'].false!(ctx)
# p x.arg_names
# p x.arg_valuess(ctx).to_a
# Fact["loves", :x, :y].arg_valuess(ctx).to_a
