# encoding: UTF-8

class String
  
  # The same as {String#downcase!} but considers Russian alphabet as well.
  # 
  # @return [String]
  # 
  def ru_downcase!
    self.tr! "A-ZА-ЯЁ", "a-zа-яё"
  end
  
  # The same as {String#downcase} but considers Russian alphabet as well.
  # 
  # @return [String]
  # 
  def ru_downcase
    self.tr "A-ZА-ЯЁ", "a-zа-яё"
  end
  
end