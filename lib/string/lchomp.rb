
class String
  
  # @param [String] prefix
  # @return [String] this {String} without +prefix+ or just this {String}
  #   if it does not have +prefix+.
  def lchomp(prefix)
    if self.start_with? prefix
    then self[prefix.size..-1]
    else self
    end
  end
  
end
