
module Enumerable
  
  def sample
    to_a.sample
  end
  
end
