
module Enumerable
  
  # @param [Proc<Proc<Object, void>, void>] each
  # @return [Enumerable] an {Enumerable} which has +each+ as implementation
  #   of its {Enumerable#each}.
  def self.new(&each)
    ProcAsEnumerable.new(&each)
  end
  
  private
  
  # @!visibility private
  class ProcAsEnumerable
    
    include Enumerable
    
    # @param [Proc<Proc<Object, void>, void>] each
    def initialize(&each)
      @each = each
    end
    
    # (see Enumerable#each)
    def each(&block)
      @each.(block)
    end
    
  end

end
