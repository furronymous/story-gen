
module Enumerable
  
  if RUBY_VERSION <= '2.0.0' then
    def lazy
      self  # just for compatibility :-[
    end
  end

end
