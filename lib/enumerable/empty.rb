
module Enumerable
  
  # @return [Boolean]
  def empty?
    each { |_| return false }
    return true
  end
  
end
