# encoding: UTF-8

# register ".sdl" extension using "pre" markup
YARD::Templates::Helpers::MarkupHelper::MARKUP_EXTENSIONS[:pre] = ['sdl']

# fix bug with extra files, when YARD generates invalid URLs
class YARD::Serializers::FileSystemSerializer
  def encode_path_components(*components)
    components.map! do |p|
      p.gsub(/[^\wа-яёА-ЯЁ\.-]/) do |x|
        encoded = '_'
        x.each_byte { |b| encoded << ("%X" % b) }
        encoded
      end
    end
  end
end